import Model.OrderList;
import View.View;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception{

        OrderList model = new OrderList();
        View view = new View();
        Controller controller = new Controller(primaryStage, model, view);
        controller.start();


    }

    public static void main(String[] args) {
        launch(args);
    }
}
