package Model;

public enum OrderStatus {
	InProcess, Placed, Shipped, Received;
}