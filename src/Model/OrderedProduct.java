package Model;

public class OrderedProduct {
	private String productID;
	private int amount;
	private double price;
	private double totalPrice;
	public Order unnamed_Order_;

	public String getProductID() {
		return this.productID;
	}

	public int getAmount() {
		return this.amount;
	}

	public double getPrice() {
		return this.price;
	}

	public double getTotalPrice() {
		return this.totalPrice;
	}
}