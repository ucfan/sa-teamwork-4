package Model;

public enum ShippingSpeed {
	Standard, TwoDay, OneDay;
}