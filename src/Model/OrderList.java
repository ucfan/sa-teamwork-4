package Model;

import java.util.ArrayList;
import java.util.List;

public class OrderList {
	private int nextID = 0;
	private List<Order> list = new ArrayList<Order>();

	public void newOrder() {
		Order newOrder = new Order();
		newOrder.setStatus(OrderStatus.InProcess);
		newOrder.setId(nextID);
		list.add(newOrder);
		nextID += 1;
	}

	public List<Order> getList() {
		return this.list;
	}

	public int getNextID() {
		return this.nextID;
	}

	public Order getOrder(int i) {
		return this.list.get(i);
	}

	public void updateOrderShippingAddr(int i, String fullName, String addressLine, String phoneNum) {
		Info info = getOrder(i).getShipping();
		info.setFullName(fullName);
		info.setAddressLine(addressLine);
		info.setAddressLine(phoneNum);
	}

}