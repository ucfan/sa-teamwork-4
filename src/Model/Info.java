package Model;

public class Info {
	private String fullName;
	private String addressLine;
	private String phoneNum;
	public Order unnamed_Order_;

	public String getFullName() {
		return this.fullName;
	}

	public String getAddressLine() {
		return this.addressLine;
	}

	public String getPhoneNum() {
		return this.phoneNum;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
}