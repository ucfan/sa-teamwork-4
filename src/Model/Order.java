package Model;

import java.util.ArrayList;
import java.util.List;

public class Order {
	private int id;
	private Info shipping = new Info();
	private Info payment = shipping;
	private ShippingSpeed speed;
	private List<OrderedProduct> items = new ArrayList<OrderedProduct>();
	private double totalPrice;
	private OrderStatus status;

	public int getId() {
		return this.id;
	}

	public Info getShipping() {
		return this.shipping;
	}

	public void setShipping(Info shipping) {
		this.shipping = shipping;
	}

	public Info getPayment() {
		return this.payment;
	}

	public void setPayment(Info payment) {
		this.payment = payment;
	}

	public ShippingSpeed getSpeed() {
		return this.speed;
	}

	public void setSpeed(ShippingSpeed speed) {
		this.speed = speed;
	}

	public double getTotalPrice() {
		return this.totalPrice;
	}

	public void calculateTotal() {
		throw new UnsupportedOperationException();
	}

	public OrderStatus getStatus() {
		return this.status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public void newItem() {
		throw new UnsupportedOperationException();
	}

	public void setId(int id) {
		this.id = id;
	}

	public void newItem(OrderedProduct product) {
		items.add(product);
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
}