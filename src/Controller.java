import Model.Order;
import Model.OrderList;
import View.View;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.xml.bind.Marshaller;

public class Controller {
    private Stage stage;

    private View view;
    private OrderList model;

    public Controller(Stage stage, OrderList model, View view) {
        this.stage = stage;
        this.model = model;
        this.view = view;
        this.view.model = model;
        this.view.thisStage = stage;
    }

    public void start() throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("fxml/SignIn.fxml"));
        stage.setTitle("Amazon Order System");
        stage.setScene(new Scene(root, 600, 400));

        stage.show();


    }
}
