package View;

import Model.Info;
import Model.Order;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.control.Label;

/**
 * Created by ucfan on 12/18/2014.
 */
public class ReviewView extends View{
    public Label lbl_Review_shipping_fullname;
    public Label lbl_Review_shipping_addressline;
    public Label lbl_Review_shipping_phonenumber;
    public Label lbl_Review_billing_phonenumber;
    public Label lbl_Review_billing_addressline;
    public Label lbl_Review_billing_fullname;

    public void Review_cancel(ActionEvent actionEvent) throws Exception {
        View.thisStage.close();
    }

    public void Review_confirm(ActionEvent actionEvent) throws Exception {
        View.thisStage.close();
    }

    public void onMouseEntered(Event event) {
        Order order = View.model.getOrder(View.model.getNextID()-1);
        Info shippingInfo = order.getShipping();
        lbl_Review_shipping_fullname.setText(shippingInfo.getFullName());
        lbl_Review_shipping_addressline.setText(shippingInfo.getAddressLine());
        lbl_Review_shipping_phonenumber.setText(shippingInfo.getPhoneNum());

        Info billingInfo = order.getPayment();
        lbl_Review_billing_fullname.setText(billingInfo.getFullName());
        lbl_Review_billing_addressline.setText(billingInfo.getAddressLine());
        lbl_Review_billing_phonenumber.setText(billingInfo.getPhoneNum());

    }
}
