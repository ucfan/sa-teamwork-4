package View;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * Created by ucfan on 12/18/2014.
 */
public class SignInView extends View {
    public void signIn_next(ActionEvent actionEvent) throws Exception {
        View.model.newOrder();
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/Shipping.fxml"));
        thisStage.setScene(new Scene(root, 600, 400));
        thisStage.show();
    }
}
