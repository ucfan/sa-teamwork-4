package View;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;

/**
 * Created by ucfan on 12/18/2014.
 */
public class ShippingView extends View {
    public TextField txt_Shipping_full_name;
    public TextField txt_Shipping_address;
    public TextField txt_Shipping_phone_number;

    public void Shipping_next(ActionEvent actionEvent) throws Exception {
        View.model.updateOrderShippingAddr(
                View.model.getNextID()-1,
                txt_Shipping_full_name.getText(),
                txt_Shipping_address.getText(),
                txt_Shipping_phone_number.getText()
        );
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/Payment.fxml"));
        View.thisStage.setScene((new Scene(root, 600, 400)));
        View.thisStage.show();
    }
}
