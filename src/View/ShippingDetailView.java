package View;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * Created by ucfan on 12/18/2014.
 */
public class ShippingDetailView extends View {
    public void ShippingDetail_next(ActionEvent actionEvent) throws Exception {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../fxml/Review.fxml"));
            View.thisStage.setScene((new Scene(root, 600, 400)));
            View.thisStage.show();
        }catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }
}
