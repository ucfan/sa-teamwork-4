package View;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * Created by ucfan on 12/18/2014.
 */
public class PaymentView extends View{
    public void Payment_next(ActionEvent actionEvent) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/ShippingDetail.fxml"));
        View.thisStage.setScene((new Scene(root, 600, 400)));
        View.thisStage.show();

    }
}
